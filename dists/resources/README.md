# JOD Meteo Web - 1.0

This JOD Distribution allow to startup and manage a JOD Agent that represent a Meteo Station that get his data from 'api.openweathermap.org' API.


## JOD Distribution Specs

This JOD Distribution was created and maintained as part of the John O.S. Project.

|||
|---|---|
| **Current version**    | 1.0
| **References**        | [JOD Meteo Web @ JOSP Docs](https://www.johnosproject.org/docs/references/jod_dists/jod_meteo_web/)
| **Repository**        | [com.robypomper.josp.jod.meteo_web @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.meteo_web/)
| **Downloads**            | [com.robypomper.josp.jod.meteo_web > Downloads @ Bitbucket](https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.meteo_web/downloads/)
<table>
  <tr><th>Current version</th><td>1.0</td></tr>
  <tr><th>References</th><td><a href="https://www.johnosproject.org/docs/references/jod_dists/jod_meteo_web/">JOD Meteo Web @ JOSP Docs</a></td></tr>
  <tr><th>Repository</th><td><a href="https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.meteo_web/">com.robypomper.josp.jod.meteo_web @ Bitbucket</a></td></tr>
  <tr><th>Downloads</th><td><a href="https://bitbucket.org/johnosproject_shared/com.robypomper.josp.jod.meteo_web/downloads/">com.robypomper.josp.jod.meteo_web > Downloads @ Bitbucket</a></td></tr>
</table>

This distribution can be installed on any computer. When started, it generates a new ```struct.jod``` file depending on the ```JOD_MWO_LOCATION``` vars defined in the ```configs/configs.sh``` file. Then it starts sharing a Meteo Station statuses to the JOSP EcoSystem like the temperature, pressure, humidity, wind speed, visibility...

In the ```configs/configs.sh``` file you can find and configure following properties:

| Property | Example | Description |
|----------|---------|-------------|
| **JOD_MWO_LOCATION** | Rome  | The english name of the location to use for query data.<br/>It must return a valid response when used in ```https://api.openweathermap.org/data/2.5/weather?q=${JOD_MWO_LOCATION}&units=metric&appid=03317c1f2de6827424efd170890ffd3c``` request |

## JOD Distribution Usage

### Locally JOD Instance

Each JOD Distribution comes with a set of script for local JOD Instance management.

| Command | Description |
|---------|-------------|
| Start    <br/>```$ bash start.sh```     | Start local JOD instance in background mode, logs can be retrieved via ```tail -f logs/console.log``` command |
| Stop     <br/>```$ bash stop.sh```      | Stop local JOD instance, if it's running |
| State    <br/>```$ bash state.sh```     | Print the local JOD instance state (obj's id and name, isRunning, PID...) |
| Install  <br/>```$ bash install.sh```   | Install local JOD instance as system daemon/service |
| Uninstall<br/>```$ bash uninstall.sh``` | Uninstall local JOD instance as system daemon/service |

### Remote JOD Instance

To deploy and manage a JOD instance on remote device (local computer, cloud server,
object device...) please use the [John Object Remote](https://www.johnosproject.org/docs/references/tools/john_object_remote/)
tools.